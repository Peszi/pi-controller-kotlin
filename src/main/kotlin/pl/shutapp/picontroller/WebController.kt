package pl.shutapp.picontroller

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.GetMapping

@Controller
internal class WebController {

    @GetMapping("/")
    fun getTestView(): String {

        return "index"
    }
}
