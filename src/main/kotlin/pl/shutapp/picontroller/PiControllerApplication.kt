package pl.shutapp.picontroller

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class PiControllerApplication

fun main(args: Array<String>) {
    runApplication<PiControllerApplication>(*args)
}

